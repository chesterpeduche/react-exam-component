import React, { useState, useEffect } from "react";
import "./customList.css";

export const CustomList = (props) => {
  const { value, searchedTerm } = props;
  const [currentItem, setCurrentItem] = useState("");

  const DetailsComponent = ({ itemDetails }) => {
    return (
      <div className="floating-details">
        <div>
          {Object.keys(itemDetails).map((keys) => {
            return (
              <div className="keys-details-handle">
                {keys} : {itemDetails[keys]}
              </div>
            );
          })}
        </div>
      </div>
    );
  };

  document.onkeydown = function (e) {
    var currentNum = currentItem;

    if (e.key === "ArrowDown") {
      currentNum =
        currentNum >= value.length - 1 ? value.length - 1 : currentNum + 1;
    } else if (e.key === "ArrowUp") {
      currentNum = currentNum <= 0 ? 0 : currentNum - 1;
    }

    setCurrentItem(currentNum);
  };

  //THE SELECT CALLBACK FUNCTION
  const itemClickFunction = (num) => {
    setCurrentItem(num);
  };

  return (
    <>
      <div className="list-component-holder">
        {value?.length &&
          searchedTerm?.length &&
          value.map((items, num) => {
            return (
              <span
                onClick={() => itemClickFunction(num)}
                key={items.sku_id}
                className={`${currentItem?.length < 0 && "list-component"} ${
                  currentItem === num && "current"
                }`}
              >
                {items.sku_name}
              </span>
            );
          })}
      </div>
      {value[currentItem] && (
        <DetailsComponent itemDetails={value[currentItem]} />
      )}
    </>
  );
};

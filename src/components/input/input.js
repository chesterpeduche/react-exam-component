import React from "react";
import "./input.css";

export const CustomInput = (props) => {
  const { value, handleChange } = props;

  return (
    <div className="input-component-holder">
      <input
        value={value}
        onChange={(text) => handleChange(text.target.value)}
        className="input-component"
      />
    </div>
  );
};

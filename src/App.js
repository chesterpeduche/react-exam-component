import "./App.css";
import { CustomInput, CustomList, Loader } from "./components";
import React, { useState, useEffect } from "react";

import useDebounce from "./hooks/useDebounce";
import { searchSkuApi } from "./api/searchSkuApi";

function App() {
  const [searchedSkuTerm, setSearchedSkuTerm] = useState();
  const [skuList, setSkuList] = useState([]);
  const [loading, setLoading] = useState();
  const [failedRequest, setFailedRequest] = useState("");

  const debouncedSearchTerm = useDebounce(searchedSkuTerm, 500);

  const getLists = async (debouncedSearchTerm) => {
    if (searchedSkuTerm && searchedSkuTerm) {
      setLoading(true);
      setSkuList([]);
      const resSkuList = await searchSkuApi(debouncedSearchTerm);

      if (resSkuList.message) {
        setFailedRequest(`${resSkuList.message}, Try again later`);
      } else {
        searchedSkuTerm && setSkuList(resSkuList);
      }

      setLoading(false);
    }
  };

  useEffect(() => {
    getLists(debouncedSearchTerm);
  }, [debouncedSearchTerm]);

  return (
    <div className="App">
      <div>Test</div>
      <div className="components-holder">
        <div className="input-holder">
          <CustomInput
            value={searchedSkuTerm}
            handleChange={setSearchedSkuTerm}
          />
        </div>
        <div className="list-holder">
          {failedRequest?.length > 0 ? (
            <div className="failed-request">{failedRequest}</div>
          ) : loading ? (
            <Loader />
          ) : (
            debouncedSearchTerm &&
            searchedSkuTerm &&
            (skuList.length ? (
              <CustomList
                value={skuList}
                searchedTerm={searchedSkuTerm}
                loading={loading}
              />
            ) : (
              <div className="not-found">NO SKU FOUND!</div>
            ))
          )}
        </div>
      </div>
    </div>
  );
}

export default App;

const api =
  "https://toro278.us-east.toroserver.com/api/demo_api_inventory/1.0/sku/search?sku_name=";

export const searchSkuApi = async (searchWord) => {
  try {
    const apiCall = await fetch(`${api}${searchWord}`);

    const { sku } = await apiCall.json();

    return sku;
  } catch (err) {
    return { message: err.message };
  }
};

import React, { useState, useEffect } from "react";

export default function useDebounce(searchValue, timer) {
  const [debouncedValue, setDebouncedValue] = useState(searchValue);

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(searchValue);
    }, timer);

    return () => {
      clearTimeout(handler);
    };
  }, [searchValue]);

  return debouncedValue;
}
